# Late Evening Colorscheme for Vim

This is a high-contrast, dark colorscheme for Vim that was forked from the `evening` colorscheme that ships with vim.

After years of use, and countless hacks to get `evening` working properly, I got fed up and remade the colorscheme, keeping the same overall feel.

**Attention has been paid to get high color terminals to have almost exactly the same colors as GUI vim.** ([example](#screenshots)) Use a terminal or GUI -- it's up to you.

Some compromises were made on 16-color terminals but that's to be expected, and won't apply to you if you use `TERM=xterm-256color`.

## Overview of Differences from `evening.vim`

### Aesthetic Fixes
- Almost black backgrounds
- `Cursorline` not so bright as to make comments disappear
- Brighter cursor
- Brighter green messages
- Nicer colors on line numbers (like comments with the current line
  highlighted if `&cursorline`)
- Replace awful-looking yellow-on-white when using `wildmenu`
- Turn delimiters white instead of orange

### Bug Fixes
- Text mode: fix invisible visual mode selection
- Fix inability to see netrw marked files when using GUI fonts that don't
  support bold (type <kbd>mf</kbd> in netrw to mark a file, controlled by
  `TabLineSel` highlight)
- Fix many inconsistencies between GUI and console (evening.vim):
  - `PreProc`: blue in console, magenta in GUI --> magenta in both
  - `Comment`: cyan in console, blue in GUI --> blue in both
  - `CursorLine`: underline in console (256 colors), gray in GUI --> gray in
    both
  - `Special`: salmon in console (256 colors), red in console (8 colors),
    orange in GUI --> salmon in both

## Screenshots

CLI:

![vim ruby (CLI)](../../raw/flair/screenshots/cli-ruby.png)

GUI:

Font: [Terminess Powerline](//github.com/powerline/fonts)

![gvim ruby (GUI)](../../raw/flair/screenshots/ruby.png)

![gvim cpp](../../raw/flair/screenshots/cpp.png)

![gvim misc](../../raw/flair/screenshots/cmd+wildmenu.png)

## Contributing

Send me a pull request!

## TODO

This hasn't been tested on MacVim, NeoVim, or PuTTY.

## License

Copyright (C) 2015 Dan Church.

License GPLv3+: GNU GPL version 3 or later (http://gnu.org/licenses/gpl.html).

This is free software: you are free to change and redistribute it.

There is NO WARRANTY, to the extent permitted by law.
